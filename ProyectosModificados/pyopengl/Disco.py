#Autor: Luis Felipe Rodriguez Espitia
#Fecha: 26/06/2020
#Clase Disco
#Descripcion: Una clase que crea un disco que rota  con la libreria glu

import pygame
from pygame.locals import *

from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *

# colores
black = (0, 0, 0, 1)
green = (0, 1, 0)


def init():
    """
    Inicializa el estado de OpenGL.
    """

    # establece el color de fondo
    glClearColor(*black)


def draw_disk():
    """
    Dibuja un disco.
    """

    # limpia el fondo
    glClear(GL_COLOR_BUFFER_BIT)

    # establece el color del disco
    glColor3f(*green)

    # dibuja el disco
    glPushMatrix

    glColor3f(1, 1, 0)

    gluDisk(gluNewQuadric(), 2,4, 127, 127)
    glPopMatrix


def main():
    pygame.init()
    display = (800, 600)
    pygame.display.set_mode(display, DOUBLEBUF | OPENGL)

    gluPerspective(45, (display[0] / display[1]), 0.1, 50.0)

    glTranslatef(0.0, 0.0, -15)

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()

        glRotatef(1, 1, 1, 1)
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

        draw_disk()
        glFlush()
        pygame.display.flip()


if __name__ == '__main__':
    main()