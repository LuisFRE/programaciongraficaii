#Autor: Luis Felipe Rodriguez Espitia
#Fecha: 26/06/2020
#Clase Cilindro
#Descripcion: Una clase que crea un cilindro que rota con la libreria glu

# importa la librería Pygame
import pygame

# importa el sub-módulo GL del módulo OpenGL
# Nota: se require la instalación de PyOpenGL

import pygame
from pygame.locals import *

from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *

# colores
black = (0, 0, 0, 1)
green = (0, 1, 0)


def init():
    """
    Inicializa el estado de OpenGL.
    """

    # establece el color de fondo
    glClearColor(*black)


def draw_Cylinder():


    # limpia el fondo
    glClear(GL_COLOR_BUFFER_BIT)

    # establece el color del cilindro
    glColor3f(*green)

    # dibuja el cilindro
    glPushMatrix

    glColor3f(1, 1, 0)
    gluCylinder(gluNewQuadric(),4,4,2,27,27)
    glPopMatrix



def main():
    pygame.init()
    display = (800, 600)
    pygame.display.set_mode(display, DOUBLEBUF | OPENGL)

    gluPerspective(45, (display[0] / display[1]), 0.1, 50.0)

    glTranslatef(0.0, 0.0, -15)

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()

        glRotatef(1, 1, 1, 1)
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)


        draw_Cylinder()
        glFlush()
        pygame.display.flip()


if __name__ == '__main__':
    main()