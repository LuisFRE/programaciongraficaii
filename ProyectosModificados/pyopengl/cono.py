#Autor: Luis Felipe Rodriguez Espitia
#Fecha: 26/06/2020
#Clase Cono
#Descripcion: Una clase que crea un cono con la libreria glu

import pygame
from pygame.locals import *

from OpenGL.GL import *
from OpenGL.GLU import *


pygame.init()
display = (400, 300)
scree = pygame.display.set_mode(display, DOUBLEBUF | OPENGL)

glEnable(GL_DEPTH_TEST)

sphere = gluNewQuadric() #Create new sphere

glMatrixMode(GL_PROJECTION)
gluPerspective(45, (display[0]/display[1]), 0.1, 50.0)

glMatrixMode(GL_MODELVIEW)
gluLookAt(0, -8, 0, 0, 0, 0, 0, 0, 1)
viewMatrix = glGetFloatv(GL_MODELVIEW_MATRIX)
glLoadIdentity()

run = True
while run:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE or event.key == pygame.K_RETURN:
                run = False

    keypress = pygame.key.get_pressed()

    # init model view matrix
    glLoadIdentity()

    # init the view matrix
    glPushMatrix()
    glLoadIdentity()

    # aplica movimientos
    if keypress[pygame.K_DOWN]:
        glTranslatef(0,0,0.1)
    if keypress[pygame.K_UP]:
        glTranslatef(0,0,-0.1)
    if keypress[pygame.K_LEFT]:
        glTranslatef(-0.1,0,0)
    if keypress[pygame.K_RIGHT]:
        glTranslatef(0.1,0,0)

    # multiply the current matrix by the get the new view matrix and store the final vie matrix
    glMultMatrixf(viewMatrix)
    viewMatrix = glGetFloatv(GL_MODELVIEW_MATRIX)

    # apply view matrix
    glPopMatrix()
    glMultMatrixf(viewMatrix)

    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT) #Clear the screen

    glPushMatrix()

    glTranslatef(1,7, -3) #Move to the place
    glColor4f(1, 0, 0, 1) #Put color

     #creacion del cono
    gluCylinder(gluNewQuadric(),4,1,6,27,27)

    glPopMatrix()

    pygame.display.flip() #Update the screen
    pygame.time.wait(10)

pygame.quit()