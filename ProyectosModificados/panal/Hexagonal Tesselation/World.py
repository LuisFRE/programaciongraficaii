#Proyecto descargado de:documentacion  oficial de OpenGl
#Modificado por: Luis Felipe Rodriguez Espitia
#Descripcion:clase dibujar la teselacion en forma de panal

from OpenGL.GL import *
from math import *
#dibujado del panal
def main():
    glEnable(GL_COLOR_MATERIAL)
    glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE)   
##    Plus = 0.86602540378443864676372317075294

    SQRT_3 = sqrt(3.0)
    dlHexagon = glGenLists(1)
    glNewList(dlHexagon, GL_COMPILE)
    glDisable(GL_TEXTURE_2D)
    glColor3f(1.0,1.0,0.0)
    glBegin(GL_QUADS)
    glNormal3f(0.0,0.0,1.0)
    glVertex3f(-1.0, 0.0, -SQRT_3)
    glVertex3f( 1.0, 0.0, -SQRT_3)
    glVertex3f( 1.0, 1.0, -SQRT_3)
    glVertex3f(-1.0, 1.0, -SQRT_3)
    glNormal3f(0.0,0.0,-1.0)
    glVertex3f(-1.0, 0.0, SQRT_3)
    glVertex3f( 1.0, 0.0, SQRT_3)
    glVertex3f( 1.0, 1.0, SQRT_3)
    glVertex3f(-1.0, 1.0, SQRT_3)
    glNormal3f(0.8660254,0.0,0.5)
    glVertex3f(-1.0, 0.0, -SQRT_3)
    glVertex3f(-1.0, 1.0, -SQRT_3)
    glVertex3f(-2.0, 1.0, 0.0)
    glVertex3f(-2.0, 0.0, 0.0)
    glNormal3f(-0.8660254,0.0,0.5)
    glVertex3f( 1.0, 0.0, -SQRT_3)
    glVertex3f( 1.0, 1.0, -SQRT_3)
    glVertex3f( 2.0, 1.0, 0.0)
    glVertex3f( 2.0, 0.0, 0.0)
    glNormal3f(0.8660254,0.0,-0.5)
    glVertex3f(-1.0, 0.0, SQRT_3)
    glVertex3f(-1.0, 1.0, SQRT_3)
    glVertex3f(-2.0, 1.0, 0.0)
    glVertex3f(-2.0, 0.0, 0.0)
    glNormal3f(-0.8660254,0.0,-0.5)
    glVertex3f( 1.0, 0.0, SQRT_3)
    glVertex3f( 1.0, 1.0, SQRT_3)
    glVertex3f( 2.0, 1.0, 0.0)
    glVertex3f( 2.0, 0.0, 0.0)
    glEnd()
    glEndList()
    
    dlWorld = glGenLists(1)
    glNewList(dlWorld, GL_COMPILE)
    glDisable(GL_TEXTURE_2D)
    glColor3f(1.0,1.0,0.0)
    zline = 0

    for z in range(58):
        zline += 1
        if zline % 2: indent = 3.0
        else: indent = 0
        glPushMatrix()
        glTranslatef(0.0,0.0,((SQRT_3+0.01)*z))
        for x in range(17):
            glPushMatrix()
            glTranslatef(6.0*x+indent,0.0,0.0)
            glCallList(dlHexagon)
            glPopMatrix()
        glPopMatrix()

    glBegin(GL_QUADS)
    glNormal3f(0,1,0)
    glVertex3i(0,0,0)
    glVertex3i(0,0,100)
    glVertex3i(100,0,100)
    glVertex3i(100,0,0)
    glEnd()

    glColor3f(1.0,1.0,1.0)
    glEnable(GL_TEXTURE_2D)
    glEndList()

    return dlWorld
