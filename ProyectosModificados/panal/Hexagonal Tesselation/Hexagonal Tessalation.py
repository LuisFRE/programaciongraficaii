#Proyecto descargado de:documentacion  oficial de OpenGl
#Modificado por: Luis Felipe Rodriguez Espitia
#Descripcion:clase principal para inicializar todos los componentes necesarios para el dibujado del panal
from OpenGL.GL import *
from OpenGL.GLU import *
import os, sys, math, random
import pygame
from pygame.locals import *
if sys.platform == 'win32' or sys.platform == 'win64':
    os.environ['SDL_VIDEO_CENTERED'] = '1'
import GL, World#importar librerias de archivos con extencion.py

pygame.init()

Screen = (800,600)
icon = pygame.Surface((1,1))
icon.set_alpha(0)
pygame.display.set_icon(icon)
pygame.display.set_caption('3D Hexagon Tessalation - v.1.0.0 - Ian Mallett - 2008')
pygame.display.set_mode(Screen,OPENGL|DOUBLEBUF)
GL.resize(Screen)
GL.luz()

CameraPos = [40.0,50.0,40.0]
CameraRotate = [-45,-45]

dlWorld = World.main()

def GetInput():
    global CameraPos, CameraRotate
    keystate = pygame.key.get_pressed()
    mrel = pygame.mouse.get_rel()
    mpress = pygame.mouse.get_pressed()
    for event in pygame.event.get():
        if event.type == QUIT or keystate[K_ESCAPE]:
            pygame.quit(); sys.exit()
        elif event.type == MOUSEBUTTONDOWN:
            ScrollSpeed = 5
            if event.button == 4: #Scroll In
                CameraPos[1] += ScrollSpeed*math.sin(math.radians(CameraRotate[1]))
                CameraPos[2] -= ScrollSpeed*math.cos(math.radians(CameraRotate[0]))*math.cos(math.radians(CameraRotate[1]))
                CameraPos[0] += ScrollSpeed*math.sin(math.radians(CameraRotate[0]))*math.cos(math.radians(CameraRotate[1]))
            elif event.button == 5: #Scroll Out
                CameraPos[1] -= ScrollSpeed*math.sin(math.radians(CameraRotate[1]))
                CameraPos[2] += ScrollSpeed*math.cos(math.radians(CameraRotate[0]))*math.cos(math.radians(CameraRotate[1]))
                CameraPos[0] -= ScrollSpeed*math.sin(math.radians(CameraRotate[0]))*math.cos(math.radians(CameraRotate[1]))
    if mpress[0]:
        Angle = math.radians(-CameraRotate[0])
        Speed = 0.1*(CameraPos[1]/50.0)
        CameraPos[0] -= (Speed*math.cos(Angle)*mrel[0])
        CameraPos[2] -= (Speed*math.sin(Angle)*-mrel[0])
        CameraPos[0] -= (Speed*math.sin(Angle)*mrel[1])
        CameraPos[2] -= (Speed*math.cos(Angle)*mrel[1])
    if mpress[2]:
        CameraRotate[0] -= 0.1*mrel[0]
        CameraRotate[1] += 0.1*mrel[1]
    if keystate[K_v]:
        CameraPos = [10,50,50]
        CameraRotate = [0,-45]
def Draw():
    #Clear
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    glLoadIdentity()
    #Camera Position/Orient
    glRotatef(-CameraRotate[1],1,0,0)
    glRotatef( CameraRotate[0],0,1,0)
    glTranslatef(-CameraPos[0],-CameraPos[1],-CameraPos[2])
    #World
    glCallList(dlWorld)
    #Flip
    pygame.display.flip()
def main():
    while True:
        GetInput()
        Draw()
if __name__ == '__main__': main()

















