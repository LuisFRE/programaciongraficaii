#Autor: Luis Felipe Rodriguez Espitia
#Fecha: 23/09/2020
#Clase Wire Tetraedro
#Descripcion: Clase para crear un tetraedro no solido con la libreria glut .

from OpenGL.GLUT import *
from OpenGL.GL import *

#Dibuja un tetraedro con glut
def draw():
    glClear(GL_COLOR_BUFFER_BIT)
    glRotate(90, 1, 1, 1)
    glutWireTetrahedron()
    glFlush()

#inicializa la ventana y llama a el metodo dibujar
def main():
    glutInit()
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB)
    glutInitWindowSize(250, 250)
    glutInitWindowPosition(100, 100)
    glutCreateWindow("My Second OpenGL Program")
    glutDisplayFunc(draw)
    glutMainLoop()

if __name__ == '__main__':
    main()