#Autor: Luis Felipe Rodriguez Espitia
#Fecha: 23/09/2020
#Clase Solid cube
#Descripcion: Clase para crear un cubo  solido con la libreria glut .

from OpenGL.GLUT import *
from OpenGL.GL import *

#metodo que dibuja un cubo solido con la libreria glut
def draw():
    glClear(GL_COLOR_BUFFER_BIT)
    glRotate(90, 0, 1, 1)
    glutSolidCube(0.5)
    glFlush()

#Inicializa los valores de la pantalla y llama a el metodo dibujar
def main():
    glutInit()
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB)
    glutInitWindowSize(250, 250)
    glutInitWindowPosition(100, 100)
    glutCreateWindow("My Second OpenGL Program")
    glutDisplayFunc(draw)
    glutMainLoop()

if __name__ == '__main__':
    main()