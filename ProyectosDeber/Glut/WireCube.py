#Autor: Luis Felipe Rodriguez Espitia
#Fecha: 23/09/2020
#Clase Wire Cube
#Descripcion: Clase para crear un cubo no solido con la libreria glut .

from OpenGL.GLUT import *
from OpenGL.GL import *
import pygame
from pygame.locals import *


#Dibuja cubo no solido con Glut
def draw():

    glClear(GL_COLOR_BUFFER_BIT)
    glRotate(1,0,1,1)
    glutWireCube(0.5)
    glFlush()

#inicializa ventana y llama a el metodo dibujar

def main():
    glutInit()
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB)
    glutInitWindowSize(250, 250)
    glutInitWindowPosition(100, 100)
    glutCreateWindow("My Second OpenGL Program")
    glRotatef(45, 3, 1, 1)
    glutDisplayFunc(draw)
    glutMainLoop()



if __name__ == '__main__':
    main()