#Autor: Luis Felipe Rodriguez Espitia
#Fecha: 23/09/2020
#Clase Solid Tetraedro
#Descripcion: Clase para crear un tetraedro  solido con la libreria glut .

from OpenGL.GLUT import *
from OpenGL.GL import *

#metodo que dibuja un tetraedro solido glut
def draw():
    glClear(GL_COLOR_BUFFER_BIT)
    glRotate(90, 0, 1, 1)
    glutSolidTetrahedron()
    glFlush()

#inicializa la ventana y llama a el metodo dibujar 
def main():
    glutInit()
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB)
    glutInitWindowSize(250, 250)
    glutInitWindowPosition(100, 100)
    glutCreateWindow("My Second OpenGL Program")
    glutDisplayFunc(draw)
    glutMainLoop()

if __name__ == '__main__':
    main()