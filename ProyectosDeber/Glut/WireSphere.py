#Autor: Luis Felipe Rodriguez Espitia
#Fecha: 23/09/2020
#Clase Wire Sphere
#Descripcion: Clase para crear una esfera no solida con la libreria glut .

from OpenGL.GLUT import *
from OpenGL.GL import *

#dibuja una esfera con glut
def draw():
    glClear(GL_COLOR_BUFFER_BIT)
    glutSwapBuffers()
    glRotate(45, 0, 1, 1)
    glutWireSphere(0.5, 100, 15)
    glFlush()


#inicializa ventana y llama a el metodo dibujar
def main():
    glutInit()
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB)
    glutInitWindowSize(250, 250)
    glutInitWindowPosition(100, 100)
    glutCreateWindow("OpenGL Program")
    glutDisplayFunc(draw)
    glutMainLoop()


if __name__ == '__main__':
    main()




