#Autor: Luis Felipe Rodriguez Espitia
#Fecha: 23/09/2020
#Clase Solid Cone
#Descripcion: Clase para crear un cono solido con la libreria glut .

from OpenGL.GLUT import *
from OpenGL.GL import *

#Metodo que dibuja un cono solido con ayuda de la libreria glut
def draw():
    glClear(GL_COLOR_BUFFER_BIT)
    glRotate(90, 1, 1, 1)
    glutSolidCone(0.2,0.8, 100, 15)
    glFlush()

#Inicializa los componentes de la pantalla llama a el metodo dibujar
def main():
    glutInit()
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB)
    glutInitWindowSize(250, 250)
    glutInitWindowPosition(100, 100)
    glutCreateWindow("My Second OpenGL Program")
    glutDisplayFunc(draw)
    glutMainLoop()

if __name__ == '__main__':
    main()