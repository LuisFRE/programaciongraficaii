#Autor: Luis Felipe Rodriguez Espitia
#Fecha: 26/06/2020
#Clase Octaedro
#Descripcion: Clase que dibuja un octaedro

from OpenGL.GL import *
from OpenGL.GLU import *
import PIRAMIDECUADRADA as pris

#dibuja un octaedro con ayuda de la clase piramide cuadrada recibiendo posicion,tamaño,rotacion y color
def draw(x,y,z,w,h,p,rx,ry,rz,r,g,b):
    glPushMatrix()
    glTranslatef(x, y, z)
    glRotatef(rx, 1, 0, 0)
    glRotatef(ry, 0, 1, 0)
    glRotatef(rz, 0, 0, 1)
    glScalef(w, h, p)


    pris.draw(0,2,0,1,1,1,0,0,0,r,g,b)
    pris.draw(0, 0, 0, 1, 1, 1,180, 0, 0, r-0.1, g-0.1, b-0.1)

    glPopMatrix()

