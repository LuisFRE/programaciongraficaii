#Autor: Luis Felipe Rodriguez Espitia
#Fecha: 26/06/2020
#Clase Cubo
#Descripcion: Clase que dibuja un cubo con la libreria glu
from OpenGL.GL import *
from OpenGL.GLU import *


#dibuja un cubo a partir de cuadrados recibiendo posicion,tamaño,rotacion y color
def draw(x,y,z,w,h,p,rx,ry,rz,r,g,b):
    glPushMatrix()
    glTranslatef(x,y,z)
    glRotatef(rx,1,0,0)
    glRotatef(ry, 0, 1, 0)
    glRotatef(rz, 0, 0, 1)
    glScalef(w,h,p)
    glColor3f(r,g,b)
    glBegin(GL_QUADS)
    glVertex3f(1.000000, 1.000000, -1.000000)
    glVertex3f(-1.000000, 1.000000, -1.000000)
    glVertex3f(-1.000000, 1.000000, 1.000000)
    glVertex3f(1.000000, 1.000000, 1.000000)
    glEnd()

    glBegin(GL_QUADS)
    glVertex3f(1.000000, -1.000000, 1.000000)
    glVertex3f(1.000000, 1.000000, 1.000000)
    glVertex3f(-1.000000, 1.000000, 1.000000)
    glVertex3f(-1.000000, -1.000000, 1.000000)
    glEnd()

    glBegin(GL_QUADS)
    glVertex3f(-1.000000, -1.000000, 1.000000)
    glVertex3f(-1.000000, 1.000000, 1.000000)
    glVertex3f(-1.000000, 1.000000, -1.000000)
    glVertex3f(-1.000000, -1.000000, -1.000000)
    glEnd()

    glBegin(GL_QUADS)
    glVertex3f(-1.000000, -1.000000, -1.000000)
    glVertex3f(1.000000, -1.000000, -1.000000)
    glVertex3f(1.000000, -1.000000, 1.000000)
    glVertex3f(-1.000000, -1.000000, 1.000000)
    glEnd()

    glBegin(GL_QUADS)
    glVertex3f(1.000000, -1.000000, -1.000000)
    glVertex3f(1.000000, 1.000000, -1.000000)
    glVertex3f(1.000000, 1.000000, 1.000000)
    glVertex3f(1.000000, -1.000000, 1.000000)
    glEnd()

    glBegin(GL_QUADS)
    glVertex3f(-1.000000, -1.000000, -1.000000)
    glVertex3f(-1.000000, 1.000000, -1.000000)
    glVertex3f(1.000000, 1.000000, -1.000000)
    glVertex3f(1.000000, -1.000000, -1.000000)
    glEnd()
    glPopMatrix()

