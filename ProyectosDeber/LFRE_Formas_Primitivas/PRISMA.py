#Autor: Luis Felipe Rodriguez Espitia
#Fecha: 26/06/2020
#Clase Prisma
#Descripcion: Clase que dibuja un prisma

from OpenGL.GL import *
from OpenGL.GLU import *


#dibuja un prisma con triangulos recibiendo posicion,tamaño,rotacion y color
def draw(x,y,z,w,h,p,rx,ry,rz,r,g,b):
    glPushMatrix()
    glTranslatef(x, y, z)
    glRotatef(rx, 1, 0, 0)
    glRotatef(ry, 0, 1, 0)
    glRotatef(rz, 0, 0, 1)
    glScalef(w, h, p)
    glColor3f(r, g, b)
    glBegin(GL_TRIANGLES)
    glVertex3f (1.0,1.0,-1.0)
    glVertex3f (-1.0,1.0,0.0)
    glVertex3f (1.0,1.0,1.0)
    glEnd()
    glBegin(GL_TRIANGLES)
    glVertex3f (-1.0,-1.0,0.0)
    glVertex3f (1.0,-1.0,-1.0)
    glVertex3f (1.0,-1.0,1.0)
    glEnd()
    glBegin(GL_TRIANGLES)
    glVertex3f (1.0,1.0,1.0)
    glVertex3f (-1.0,-1.0,0.0)
    glVertex3f (1.0,-1.0,1.0)
    glEnd()
    glBegin(GL_TRIANGLES)
    glVertex3f (1.0,1.0,-1.0)
    glVertex3f (1.0,-1.0,1.0)
    glVertex3f (1.0,-1.0,-1.0)
    glEnd()
    glBegin(GL_TRIANGLES)
    glVertex3f (-1.0,1.0,0.0)
    glVertex3f (1.0,-1.0,-1.0)
    glVertex3f (-1.0,-1.0,0.0)
    glEnd()
    glBegin(GL_TRIANGLES)
    glVertex3f (1.0,1.0,1.0)
    glVertex3f (-1.0,1.0,0.0)
    glVertex3f (-1.0,-1.0,0.0)
    glEnd()
    glBegin(GL_TRIANGLES)
    glVertex3f (1.0,1.0,-1.0)
    glVertex3f (1.0,1.0,1.0)
    glVertex3f (1.0,-1.0,1.0)
    glEnd()
    glBegin(GL_TRIANGLES)
    glVertex3f (-1.0,1.0,0.0)
    glVertex3f (1.0,1.0,-1.0)
    glVertex3f (1.0,-1.0,-1.0)
    glEnd()
    glPopMatrix()