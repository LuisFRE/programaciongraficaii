#Autor: Luis Felipe Rodriguez Espitia
#Fecha: 26/06/2020
#Clase Cono
#Descripcion: Clase que dibuja un cono con la libreria glu

from OpenGL.GL import *
from OpenGL.GLU import *


#dibuja un cono recibiendo ,posicion,tamaño,rotacion y forma
def draw(x,y,z,w,h,p,rx,ry,rz,r,g,b):
    glPushMatrix()
    glTranslatef(x,y,z)
    glRotatef(rx,1,0,0)
    glRotatef(ry, 0, 1, 0)
    glRotatef(rz, 0, 0, 1)
    glScalef(w,h,p)
    glColor3f(r,g,b)


    gluCylinder(gluNewQuadric(), 1, 0, 3, 10, 10);
    glPopMatrix()