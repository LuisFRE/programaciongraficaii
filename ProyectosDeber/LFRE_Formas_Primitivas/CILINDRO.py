#Autor: Luis Felipe Rodriguez Espitia
#Fecha: 26/06/2020
#Clase Cilindro
#Descripcion: Clase que dibuja un cilindro con la libreria glu

from OpenGL.GL import *
from OpenGL.GLU import *

#dibuja un cilindro recibiendo la posicion,tamaño,rotacion y color

def draw(x,y,z,w,h,p,rx,ry,rz,r,g,b):
    glPushMatrix()
    glTranslatef(x,y,z)
    glRotatef(rx,1,0,0)
    glRotatef(ry, 0, 1, 0)
    glRotatef(rz, 0, 0, 1)
    glScalef(w,h,p)
    glColor3f(r,g,b)
    gluQuadricDrawStyle(gluNewQuadric(), GLU_FILL);
    gluCylinder(gluNewQuadric(), 1, 1, 2, 25, 25)
    glPopMatrix()

