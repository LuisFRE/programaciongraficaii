#Autor: Luis Felipe Rodriguez Espitia
#Fecha: 26/06/2020
#Clase Esfera
#Descripcion: Clase que dibuja una esfera con la libreria glu

from OpenGL.GL import *
from OpenGL.GLU import *

#dibuja una esfera recibiendo posicion,tamaño,rotacion y color

def draw(x,y,z,w,h,p,rx,ry,rz,r,g,b):
    glPushMatrix()
    glTranslatef(x,y,z)
    glRotatef(rx,1,0,0)
    glRotatef(ry, 0, 1, 0)
    glRotatef(rz, 0, 0, 1)
    glScalef(w,h,p)
    glColor3f(r,g,b)


    gluSphere(gluNewQuadric(), 2, 27, 27)
    glPopMatrix()

