#Autor: Luis Felipe Rodriguez Espitia
#Fecha: 26/06/2020
#Clase Luces
#Descripcion: Una clase que ayuda a manejar luces cambiando la posicion de acuerdo a el teclado


from OpenGL.GL import *
from OpenGL.GLU import *
#inicializa los valores de la pantalla
def IniciarPantalla(screen):
    width = screen[0]
    height = screen[1]
    if height == 0:
        height = 1
    glViewport(0, 0, width, height)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(45, 1.0 * width / height, 1.0, 10000.0)
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()
#Metodo para crear luces recibiendo la posisicon de las mismas
def IniciarIluminacion(x,y,z):
    glEnable(GL_BLEND)  # masking functions
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE)

    glShadeModel(GL_SMOOTH)
    glEnable(GL_TEXTURE_2D)
    # Colores de fondo
    #glClearColor(0.27, 0.45, 0.9, 0.0) # Celeste
    #glClearColor(0.5, 0.5, 0.5, 0.0) # Gris
    glClearColor(0.0, 0.0, 0.0, 0.0) # Negro
    glClearDepth(1.0)
    glEnable(GL_DEPTH_TEST)
    glEnable(GL_ALPHA_TEST)
    glDepthFunc(GL_LEQUAL)
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST)

    glAlphaFunc(GL_NOTEQUAL, 0.0)

    # glFogfv(GL_FOG_DENSITY, .01)
    # glFogfv(GL_FOG_COLOR, (1,1,1,0.5))
    # glEnable(GL_FOG)

    # Par metros de color de luz
    LightAmbient = [0.0, 0.0, 0.0, 1.0]
    LightDiffuse = [1, 1, 1, 1.0]
    LightSpecular = [0.0, 1.0, 0.4, 1.0]
    #posicion de la luz en el escenario
    LightPosition = [x, y, z, 1.0]
    PosicionAnguloLuz = [0,0,0]
    brillo=80.0
   # LightAmbient = [0, 0.1, 0.0]
   # LightDiffuse = [0.1, 0.1, 0.1]
   # LightIntensity = [0.0, 0.0, 0.0]
    #LightPosition = [x,y,z, 1]
    # Tipos de luces
    glEnable(GL_LIGHTING)
    #glEnable(GL_COLOR_MATERIAL)
    glEnable(GL_LIGHT0)
    #parametros necesarios para la crecion de luces
    glLightfv(GL_LIGHT0, GL_POSITION, LightPosition)
    glLightfv(GL_LIGHT0, GL_AMBIENT, LightAmbient)
    glLightfv(GL_LIGHT0, GL_DIFFUSE, LightDiffuse)
    glLightfv(GL_LIGHT0, GL_SPECULAR, LightSpecular)
    glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, PosicionAnguloLuz)
    #glLightfv(GL_LIGHT0, GL_SHININESS, brillo)
    glLight(GL_LIGHT0, GL_CONSTANT_ATTENUATION, 0.3)
    glLight(GL_LIGHT0, GL_LINEAR_ATTENUATION, 0.5)
    glLight(GL_LIGHT0, GL_QUADRATIC_ATTENUATION, 0.4)
    glLight(GL_LIGHT0, GL_SPOT_EXPONENT, 80)






