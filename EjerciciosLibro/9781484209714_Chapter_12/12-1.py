#Proyecto descargado de:https://github.com/apress/beg-python-games-dev-2ed
#Modificado por: Luis Felipe Rodriguez Espitia
##Descripcion:Clase que crea un constructor

def get_attenuation(distance, constant, linear, quadratic):
    return 1.0 / (constant + linear * distance + quadratic * (distance ** 2))
