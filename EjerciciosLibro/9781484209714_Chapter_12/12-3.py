#Proyecto descargado de:https://github.com/apress/beg-python-games-dev-2ed
#Modificado por: Luis Felipe Rodriguez Espitia
##Descripcion:Clase que crea un constructor

def additive_blend(src, dst):
    return src * src.a + dst
