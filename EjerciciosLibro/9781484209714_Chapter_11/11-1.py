#Proyecto descargado de:https://github.com/apress/beg-python-games-dev-2ed
#Modificado por: Luis Felipe Rodriguez Espitia

from math import log, ceil
def next_power_of_2(size):
    return 2 ** ceil(log(size, 2))
